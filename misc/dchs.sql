-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2015 at 05:16 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dchs`
--
CREATE DATABASE IF NOT EXISTS `dchs` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dchs`;

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examinee_id` int(11) NOT NULL,
  `answers` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `examinee_id`, `answers`) VALUES
(2, 6, '{"13":"B","14":"C","15":"B"}'),
(3, 7, '{"19":"C","20":"B"}'),
(4, 8, '{"25":"A","26":"A","27":"B","28":"A","29":"A"}'),
(5, 10, '{"25":"D","26":"A","27":"A","28":"C","29":"A"}');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Biology'),
(2, 'Mathematics');

-- --------------------------------------------------------

--
-- Table structure for table `examinees`
--

CREATE TABLE IF NOT EXISTS `examinees` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `exam_id` int(10) NOT NULL,
  `score` int(4) NOT NULL,
  `user_id` int(10) NOT NULL,
  `address` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(8) NOT NULL,
  `status` varchar(30) NOT NULL,
  `ratings` tinyint(4) NOT NULL,
  `start_time` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `year` int(5) NOT NULL,
  `finished_time` datetime NOT NULL,
  `password` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `examinees`
--

INSERT INTO `examinees` (`id`, `exam_id`, `score`, `user_id`, `address`, `birthday`, `gender`, `status`, `ratings`, `start_time`, `date_added`, `year`, `finished_time`, `password`) VALUES
(1, 0, 0, 0, 'test', '2004-02-02', 'M', 'Active', 0, '0000-00-00 00:00:00', '2015-10-06 09:28:34', 2015, '0000-00-00 00:00:00', '8zj31g'),
(6, 3, 0, 20, 'test', '1992-04-16', 'F', 'Active', 0, '2015-09-27 16:23:00', '2015-09-27 10:33:53', 2015, '2015-09-27 16:44:00', 'snyu1z'),
(7, 3, 0, 21, 'l', '1994-06-17', 'M', 'Active', 0, '2015-09-27 17:38:00', '2015-09-27 17:37:26', 2015, '2015-09-27 17:39:00', '9lwzwx'),
(8, 1, 0, 22, '3', '2003-02-02', 'M', 'Active', 0, '2015-10-06 09:40:00', '2015-10-06 09:33:26', 2015, '2015-10-06 09:40:00', 'cn19xp'),
(9, 1, 0, 23, '3', '2004-05-02', 'M', 'Active', 0, '0000-00-00 00:00:00', '2015-10-07 10:44:05', 2015, '0000-00-00 00:00:00', 'rudmqm'),
(10, 1, 0, 24, '4', '2004-04-02', 'M', 'Active', 0, '2015-10-07 10:45:00', '2015-10-07 10:44:33', 2015, '2015-10-07 10:45:00', '6d0ej7');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE IF NOT EXISTS `exams` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `proctor_id` int(10) NOT NULL,
  `schedule` datetime NOT NULL,
  `description` text NOT NULL,
  `no_of_examinees` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `proctor_id`, `schedule`, `description`, `no_of_examinees`) VALUES
(1, 3, '2015-10-31 09:28:00', 'Test', 3),
(3, 3, '2015-09-28 11:30:00', 'Exam 2015\r\n', 20),
(4, 3, '2015-09-30 10:58:00', 'Exam 2016\r\n', 20);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `choice1` varchar(50) NOT NULL,
  `choice2` varchar(50) NOT NULL,
  `choice3` varchar(50) NOT NULL,
  `choice4` varchar(50) NOT NULL,
  `answer` varchar(2) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `question`, `choice1`, `choice2`, `choice3`, `choice4`, `answer`, `questionnaire_id`, `category_id`) VALUES
(19, 'Math', '1', '1', '1', '1', 'A', 17, 6),
(20, '2', '2', '2', '2', '2', 'A', 17, 7),
(21, 'Math', '1', '1', '1', '1', 'A', 19, 6),
(22, '2', '2', '2', '2', '2', 'A', 19, 7),
(23, 'k', '1k', 'k', 'k', 'k', 'A', 20, 6),
(24, 'k', 'k', 'k', 'k', 'k', 'A', 20, 7),
(25, 'Math', '1', '1', '1', '1', 'A', 21, 6),
(26, '2', '2', '2', '2', '2', 'A', 21, 7),
(27, '1', '1', '2', '3', '4', 'A', 21, 2),
(28, '4', '4', '5', '5', '5', 'A', 21, 2),
(29, '6', '6', '6', '6', '6', 'A', 21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `numberings`
--

CREATE TABLE IF NOT EXISTS `numberings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=353 ;

--
-- Dumping data for table `numberings`
--

INSERT INTO `numberings` (`id`, `questionnaire_id`, `item_id`, `number`) VALUES
(254, 20, 23, 1),
(255, 20, 24, 2),
(256, 19, 21, 1),
(257, 19, 22, 2),
(264, 17, 19, 1),
(265, 17, 20, 2),
(348, 21, 28, 1),
(349, 21, 25, 2),
(350, 21, 27, 3),
(351, 21, 29, 4),
(352, 21, 26, 5);

-- --------------------------------------------------------

--
-- Table structure for table `proctors`
--

CREATE TABLE IF NOT EXISTS `proctors` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `middle_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `proctors`
--

INSERT INTO `proctors` (`id`, `first_name`, `middle_name`, `last_name`) VALUES
(3, 'John', 'Oh My', 'Doe'),
(4, 'Jay Anne', 'O', 'Bautista'),
(5, 'Trixie', 'C.', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `questionnaires`
--

CREATE TABLE IF NOT EXISTS `questionnaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `exam_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `questionnaires`
--

INSERT INTO `questionnaires` (`id`, `name`, `date_added`, `date_modified`, `exam_id`) VALUES
(17, 'New', '2015-09-27 17:30:00', '0000-00-00 00:00:00', 3),
(19, 'Exam 2015', '2015-09-27 17:32:00', '0000-00-00 00:00:00', 3),
(20, 'Lost', '2015-09-27 17:35:00', '0000-00-00 00:00:00', 3),
(21, 'New Test', '2015-10-06 09:33:00', '2015-10-06 09:34:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `username`, `password`) VALUES
(1, 'Jane', 'M', 'Doe', 'admin', 'a5229871d039c7d28998ce536bf96fd1a4ea65d9'),
(12, 'test', 'test', 'test', 'test.test_wqxej', 'a9f92d710dba6b459bec76aa76b36c7cea1efb32'),
(14, 'Jay Anne', 'Ol', 'Bautista', '2015-0919072838', '451b6410388a48dea2981ea34a08a19200a41a74'),
(15, 'jj', 'jj', 'jj', 'jj123', '5966a2e2197ec971ce05dffe175bf77529ea9d99'),
(16, 'lara', 'lara', 'lara', 'lara123', '343b40eaa0afd442161c904c49ebd4e8fef3066b'),
(17, 'user', 'user', 'user', 'user.user_3x48s', '079f63119797cca8b95c22b675dd508631e4f096'),
(18, 'lol', 'lol', 'lol', 'lol.lol_dhjcp', '5932cfbf803e34cec8c54e9d7a5fe6c48c3f1c8b'),
(19, 'pass', 'pass', 'pass', 'pass.pass_geax9', '13119a9ea66766ea5df6bc1b6d6ac7e9d46b9e88'),
(20, 'test', 'test', 'test', 'test.test_0nmvc', '7ee4bd23009e537fcb5039097bbd6feb7a3b213c'),
(21, 'l', 'l', 'l', 'l.l_5w749', '2b10298593f88ba84e1a0583291f64e6235f1a3c'),
(22, '3', '3', '3', '3.3_naxtr', 'e274242a64d119b26393d25fbc0499e1290026b5'),
(23, '3', '3', '3', '3.3_aq4bg', '083a2106466c01cda024c6243f05cd06673b627e'),
(24, '4', '4', '4', '4.4_q1pd8', '1bb68571d9d6237f4f905a2490fcda3484f70b4d');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
