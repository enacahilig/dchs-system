<?php
/*all the functions for examinees add here*/

App::uses('AppController', 'Controller');
class ExamineesController extends AppController {
	public function index(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('User');
		$this->loadModel('Exam');
		$examinees = $this->Examinee->find('all');

		$this->loadModel("Questionnaire");
		$this->loadModel("Answer");


		//solve scores
		foreach ($examinees as $index => $examinee) {
			$questionnaire = $this->Questionnaire->findByExamId($examinee["Exam"]["id"]);
			$answers = $this->Answer->findByExamineeId($examinee["Examinee"]["id"]);
			if($answers){
				$answers = json_decode($answers["Answer"]["answers"], true);
			}

			$score=0;
			$questionnaire["Item"] = isset($questionnaire["Item"]) ? $questionnaire["Item"] : array();
			$total = count($questionnaire["Item"]);
			foreach ($questionnaire["Item"] as $key => $item) {
				$examinee_answer = isset($answers[$item["id"]]) ? $answers[$item["id"]] : ''; 
				if( $item["answer"] == $examinee_answer ){
					$score += 1;
				}

			}
			$examinees[$index]["Examinee"]["score"] = $score."/".$total;
		}

		$this->set("examinees", $examinees);		
	}
	
	public function add(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('User');
		$this->loadModel('Exam');
		$today = date('Y-m-d H:i');
		$conditions = "Exam.schedule > '$today'";
		$exams  = $this->Exam->find('all', compact('conditions'));//check for exams with future dates

		$this->Exam->virtualFields['descriptionAndName'] = "CONCAT(Exam.description, ' - ', DATE_FORMAT(Exam.schedule,'%m/%d/%Y %h:%i'))";
		$fields ="descriptionAndName";
		$schedules = $this->Exam->find("list", array("fields"=>array("Exam.id", "descriptionAndName"), "conditions"=>$conditions));
		$this->set("schedules", $schedules);
		if($exams){
			if($this->request->is("post")){
				$exam_id = $this->request->data['Examinee']['exam_id'];
				$total_examinees = $this->Examinee->find("count", array( "conditions" => "Examinee.exam_id  = $exam_id"));
				$is_available = $total_examinees < $this->Exam->findById($exam_id)['Exam']['no_of_examinees'];
				

				if($is_available){
					$first_name = $this->request->data['Examinee']['first_name'];
					$middle_name = $this->request->data['Examinee']['middle_name'];
					$last_name = $this->request->data['Examinee']['last_name'];
					$password = $this->request->data['Examinee']['password'];
					$date_added = date("Y-m-d H:i:s");
					$year = date("Y");
					$birthday = $this->request->data['Examinee']['year'].'-'.$this->request->data['Examinee']['month'].'-'.$this->request->data['Examinee']['day'];
					
			
					$user_name = $this->request->data['Examinee']['user_name'];
					
					$this->User->create();
					$this->User->save(
						array(
							'first_name'=>$first_name,
							'middle_name'=>$middle_name,
							'last_name'=>$last_name,
							'username'=>$user_name,
							'password'=>$password,

					));
					
					$user_id =$this->User->findByUsername($user_name);
					
					
					$this->Examinee->create();
					$this->Examinee->save(
						array(
							'user_id' => $user_id['User']['id'],
							'address' => $this->request->data['Examinee']['address'],
							'birthday'=> $birthday,
							'gender' => $this->request->data['Examinee']['gender'],
							'status' => $this->request->data['Examinee']['status'],
							'date_added' => $date_added,
							'year' => $year,
							'exam_id' => $this->request->data['Examinee']['exam_id'],
							'password'=>$password


					));
					
					$this->Session->setFlash(__('The examinee was successfully added.'), 'default', array('class' => 'alert alert-success'));
					$this->redirect('/examinees');
				}
				else{
					$this->Session->setFlash(__('This exam schedule is already full, you can add the examinee to other schedules or adjust the number of examinees.'), 'default', array('class' => 'alert alert-danger'));
					$this->redirect('/examinees');
				}

			}
				

		}
		else{
			$this->Session->setFlash(__('Please add an exam before adding examinees.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect('/examinees');
		}
		
		



	}
	public function view($id){
		$this->loadModel('User');
		$examinee = $this->Examinee->findById($id);
		$this->set("examinee", $examinee);
		$this->request->data = $examinee;



	}
	public function edit($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('User');
		$this->loadModel('Exam');
		$today = date('Y-m-d H:i');
		$conditions = "Exam.schedule > '$today'";

		$this->Exam->virtualFields['descriptionAndName'] = "CONCAT(Exam.description, ' - ', DATE_FORMAT(Exam.schedule,'%m/%d/%Y %h:%i'))";
		$fields ="descriptionAndName";
		$schedules = $this->Exam->find("list", array("fields"=>array("Exam.id", "descriptionAndName"), "conditions"=>$conditions));
		$this->set("schedules", $schedules);

		if($this->request->is("post") || $this->request->is("put")){
			if($this->Examinee->exists($id)){
				$exam_id = $this->request->data['Examinee']['exam_id'];
				$total_examinees = $this->Examinee->find("count", array( "conditions" => "Examinee.exam_id  = $exam_id"));
				$is_available = $total_examinees < $this->Exam->findById($exam_id)['Exam']['no_of_examinees'];
				if($is_available){
					$examinee = $this->Examinee->findById($id);
					$examinee['User']['first_name'] = $this->request->data['Examinee']['first_name'];
					$examinee['User']['middle_name'] = $this->request->data['Examinee']['middle_name'];
					$examinee['User']['last_name'] = $this->request->data['Examinee']['last_name'];
					$examinee['User']['username'] = $this->request->data['Examinee']['user_name'];
					$examinee['Examinee']['date_added'] =  date("Y-m-d H:i:s");
					$examinee['Examinee']['year'] = date("Y");
					$examinee['Examinee']['birthday'] = $this->request->data['Examinee']['year'].'-'.$this->request->data['Examinee']['month'].'-'.$this->request->data['Examinee']['day'];
					$examinee['Examinee']['gender'] = $this->request->data['Examinee']['gender'] ? "Female" : "Male";
					$examinee['Examinee']['exam_id'] = $this->request->data['Examinee']['exam_id'];
					
					$this->User->save($examinee);
					$this->Examinee->save($examinee);
					$this->Session->setFlash(__('The examinee was successfully updated.'), 'default', array('class' => 'alert alert-success'));
					$this->redirect("/examinees/view/{$id}");
				}
				else{
			
					$this->Session->setFlash(__('This exam schedule is already full, you can add the examinee to other schedules or adjust the number of examinees.'), 'default', array('class' => 'alert alert-danger'));
					

				}
				
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			
		}
		$examinee = $this->Examinee->findById($id);
		$this->set("examinee", $examinee);
		$this->request->data = $examinee;
		
		

	}

	public function delete($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('User');
		if($this->Examinee->exists($id)){

			
			$this->Examinee->id = $id;
			$user = $this->Examinee->find('first', array("conditions" => "Examinee.id=$id"));
		
			$this->User->delete($user['User']['id']);
			$this->Examinee->delete();
			$this->Session->setFlash(__('The examinee was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/examinees');
	}

	public function search(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$keyword = isset($this->data['Examinee']['keyword'])?$this->data['Examinee']['keyword']:'';
		$this->loadModel('User');
		$conditions = "Examinee.id LIKE '%$keyword%' OR User.username LIKE '%$keyword%' OR User.first_name LIKE '%$keyword%' OR User.last_name  LIKE '%$keyword%' OR CONCAT(User.first_name,' ', User.last_name)LIKE '%$keyword%' OR User.middle_name OR CONCAT(User.first_name,' ', User.middle_name, ' ',User.last_name)LIKE '%$keyword%'";
		$examinees = $this->Examinee->find('all', compact('conditions'));
		$this->set("examinees", $examinees);
		

		
	}
	public function upload_image($id){//problrm
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));

		if ($this->request->is('post')) {
			$uploaded_image = WideImage::load($_FILES["fileToUpload"]["tmp_name"]);

			$uploaded_image->resize(200,200,'fill')->saveToFile(WWW_ROOT."img".DS."examinees".DS.$id.".jpg");
		}
		$this->redirect("/examinees/view/{$id}/refresh:".rand(1000,9999));
	}

	public function take_exam(){
		$this->loadModel("User");
		$user = $this->User->findById($this->Auth->user('id'));
		$examinee = $this->Examinee->findById($user["Examinee"]["id"]);

		if($examinee["Examinee"]["start_time"]!="0000-00-00 00:00:00"){
			$this->redirect("start");
		}
	}

	public function start(){
		$this->loadModel("User");
		$user = $this->User->findById($this->Auth->user('id'));
		$examinee = $this->Examinee->findById($user["Examinee"]["id"]);
		$this->set("examinee", $examinee);

		if($examinee["Examinee"]["start_time"]=="0000-00-00 00:00:00"){
			$this->Examinee->id = $user["Examinee"]["id"];
			$this->Examinee->saveField("start_time", date("Y-m-d H:i"));
			
		}

		if($examinee["Examinee"]["finished_time"]!="0000-00-00 00:00:00"){
			$this->redirect("submit");
		}

		$examinee = $this->Examinee->findById($user["Examinee"]["id"]);

		//solve finishing time
		$date = new DateTime($examinee["Examinee"]["start_time"]);
		$date->modify("+90 minutes");
		$this->set("end_date", $date->format('Y-m-d h:i A'));

		$this->loadModel("Questionnaire");
		$questionnaire = $this->Questionnaire->findByExamId($examinee["Exam"]["id"]);
		$this->set("questionnaire", $questionnaire);

		$this->loadModel("Item");
		$conditions = "Item.questionnaire_id={$questionnaire["Questionnaire"]["id"]}";
		$order = "Numbering.number ASC";	
		$items = $this->Item->find("all", compact("conditions", "order"));
		$this->set("items", $items);
	}

	function ajax_save_answers($id){
		$answers = isset($_POST['answers']) ? $_POST['answers'] : array();
	
		$this->loadModel("Answer");

		$answers_exist = $this->Answer->findByExamineeId($id);

		if($answers_exist){
			$this->Answer->id = $answers_exist["Answer"]["id"];
			$this->Answer->save(array("examinee_id"=>$id, "answers"=>$_POST['answers'], "id"=>$answers_exist["Answer"]["id"]));
		}
		else{
			$this->Answer->create();
			$this->Answer->save(array("examinee_id"=>$id, "answers"=>$_POST['answers']));
		}
		
		$this->layout="ajax";
		exit; 
	}

	function submit(){
		$this->loadModel("User");
		$user = $this->User->findById($this->Auth->user('id'));
		$examinee = $this->Examinee->findById($user["Examinee"]["id"]);
		
		if($examinee["Examinee"]["finished_time"]=="0000-00-00 00:00:00"){
			$this->Examinee->id = $user["Examinee"]["id"];
			$this->Examinee->saveField("finished_time", date("Y-m-d H:i"));	
		}
	}

	public function scoresheet($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee

		$examinee = $this->Examinee->findById($id);
		$this->set("examinee", $examinee);

		//get quesitonnaire
		$this->loadModel("Questionnaire");
		$questionnaire = $this->Questionnaire->findByExamId($examinee["Exam"]["id"]);

		$answers = isset($examinee["Answer"]["answers"]) ? json_decode($examinee["Answer"]["answers"],true) : array();

		//solve scores
		$score=0;

		$this->loadModel("Category");
		$categories = $this->Category->find("all");

		$category_scores = [];

		foreach ($questionnaire["Item"] as $key => $item) {
			$examinee_answer = isset($answers[$item["id"]]) ? $answers[$item["id"]] : '';
			//save the examinee's anwer to array 
			$questionnaire["Item"][$key]["exam_answer"] = $examinee_answer;
			if( $item["answer"] == $examinee_answer ){
				$score += 1;
				if(isset($category_scores[$item["category_id"]])){
					$category_scores[$item["category_id"]] += 1;
				}
				else{
					$category_scores[$item["category_id"]] = 1;
				}
			}
		}

		//display scores
		$display_scores = [];
		foreach ($category_scores as $category_id => $scores) {
			$category = $this->Category->findById($category_id);
			$display_scores[$category["Category"]["name"]] = $scores; 
		}

		/*if($examinee["Examinee"]["finished_time"]=="0000-00-00 00:00:00"){
			$this->Examinee->id = $examinee["Examinee"]["id"];
			$this->Examinee->saveField("finished_time", date("Y-m-d H:i"));
		}*/
		
		$this->set("score", $score);
		$this->set("total_items", count($questionnaire["Item"]));
		$this->set("display_scores", $display_scores);
		$this->set("questionnaire", $questionnaire);
	}

}
;?>