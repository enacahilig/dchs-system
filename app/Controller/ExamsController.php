<?php
/*all the functions for exams add here*/

App::uses('AppController', 'Controller');
class ExamsController extends AppController {

	public function index(){

		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('Proctor');
		$this->loadModel('Examinee');
		
		$exams = $this->Exam->find("all", array('order' => array('Exam.schedule' => 'DESC')));
		$this->set("exams", $exams);

		

	
	}
	public function add(){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('Proctor');
		$this->Proctor->virtualFields['full_name'] =  'CONCAT(Proctor.first_name, " ", Proctor.last_name)';
		
		$proctors = $this->Proctor->find("list", array("fields"=>array("Proctor.id", "full_name")));


		$this->set("proctors", $proctors);
		if($this->request->is("post")){
			$schedule = date('Y-m-d', strtotime($this->request->data['Exam']['schedule'])).' '. date('H:i', strtotime($this->request->data['Exam']['time']));
			
			if($schedule > date('Y-m-d H:i')){
				$this->request->data['Exam']['schedule'] = $schedule;
				$this->Exam->create();
				$this->Exam->save($this->request->data);
				$this->Session->setFlash(__('The exam was successfully added.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect('/exams');

			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			
		}
		
	}

	public function delete($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->Exam->exists($id)){
			$this->Exam->id = $id;
			$this->Exam->delete();
			$this->Session->setFlash(__('The exam was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/exams');
	}
	public function edit($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('Proctor');
		$this->Proctor->virtualFields['full_name'] =  'CONCAT(Proctor.first_name, " ", Proctor.last_name)';
	
		$exam = $this->Exam->findById($id);
		$this->set('exam', $exam);
		$proctors = $this->Proctor->find("list", array("fields"=>array("Proctor.id", "full_name")));
		$this->set("proctors", $proctors);
		if($this->request->is("post") || $this->request->is("put")){
			$schedule = date('Y-m-d', strtotime($this->request->data['Exam']['schedule'])).' '. date('H:i', strtotime($this->request->data['Exam']['time']));
			
			if($schedule > date('Y-m-d H:i')){
				$this->request->data['Exam']['schedule'] = $schedule;
				$this->Exam->id = $id;
				
				$this->Exam->save($this->request->data);
				$this->Session->setFlash(__('The exam was successfully updated.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect('/exams');

			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			
		}

	}
	public function view($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('Proctor');
		$this->loadModel('User');
		$this->loadModel('Examinee');
		
		$exam = $this->Exam->findById($id);
		$this->set('exam', $exam);
		$examinees = array();// array for list of examinees
		$added_examinees  =  $this->Examinee->find("count", array("conditions"=> "Examinee.exam_id = $id"));
		$this->set('added_examinees', $added_examinees);
		
		foreach ($exam['Examinee'] as $key => $examinee) {

			$users = $this->User->findById($examinee['user_id']);//access examinee name
			$examinees[] = $users;// store to array for view
			

			
		}

		$this->loadModel("Questionnaire");
		$this->loadModel("Answer");

		foreach ($examinees as $index => $examinee) {
			$questionnaire = $this->Questionnaire->findByExamId($examinee["Examinee"]["exam_id"]);
			$answers = $this->Answer->findByExamineeId($examinee["Examinee"]["id"]);
			if($answers){
				$answers = json_decode($answers["Answer"]["answers"], true);
			}

			//solve scores
			$score=0;
			$questionnaire["Item"] = isset($questionnaire["Item"]) ? $questionnaire["Item"] : array();
			$total = count($questionnaire["Item"]);
			foreach ($questionnaire["Item"] as $key => $item) {
				$examinee_answer = isset($answers[$item["id"]]) ? $answers[$item["id"]] : ''; 
				if( $item["answer"] == $examinee_answer ){
					$score += 1;
				}

			}
			$examinees[$index]["Examinee"]["score"] = $score."/".$total;
		}

		$this->set('examinees', $examinees);

		
		



	}
	public function export_exam($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$this->loadModel('Proctor');
		$this->loadModel('User');
		$this->loadModel('Examinee');
		
		$exam = $this->Exam->findById($id);
		$this->set('exam', $exam);
		$examinees = array();// array for list of examinees
		
		
		foreach ($exam['Examinee'] as $key => $examinee) {

			$users = $this->User->findById($examinee['user_id']);//access examinee name
			$examinees[] = $users;// store to array for view
			

			
		}

		$this->loadModel("Questionnaire");
		$this->loadModel("Answer");

		foreach ($examinees as $index => $examinee) {
			$questionnaire = $this->Questionnaire->findByExamId($examinee["Examinee"]["exam_id"]);
			$answers = $this->Answer->findByExamineeId($examinee["Examinee"]["id"]);
			if($answers){
				$answers = json_decode($answers["Answer"]["answers"], true);
			}

			//solve scores
			$score=0;
			$questionnaire["Item"] = isset($questionnaire["Item"]) ? $questionnaire["Item"] : array();
			$total = count($questionnaire["Item"]);
			foreach ($questionnaire["Item"] as $key => $item) {
				$examinee_answer = isset($answers[$item["id"]]) ? $answers[$item["id"]] : ''; 
				if( $item["answer"] == $examinee_answer ){
					$score += 1;
				}

			}
			$examinees[$index]["Examinee"]["score"] = $score." out of ".$total;
		}



		header('Content-Type: application/excel');
     	header('Content-Disposition: attachment; filename="dchs_examineelist.csv"');
     	$fp = fopen('php://output', 'w');
     	
     	$dchs = array(
        		'',
        		'',
        		'School: DCHS',
        );

        fputcsv($fp, $dchs);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);
 		fputcsv($fp, array());

     	$proctor = 'Proctor: '.$exam["Proctor"]["first_name"].' '.$exam["Proctor"]["last_name"];
     	$schedule = 'Schedule: '.date('F d, Y h:sA', strtotime($exam["Exam"]["schedule"]));
     	$no_of_examinees ='No. of Examinees: '.$exam['Exam']['no_of_examinees'];
     	$total = 'Total Examinees Added: '.count($exam['Examinee']);
     	$exam_row_1 = array(
     		$proctor,
     		$schedule
		);
		fputcsv($fp, $exam_row_1);

		$exam_row_2 = array(
			$no_of_examinees,
			$total
			
		);
		fputcsv($fp, $exam_row_2);
		fputcsv($fp, array());
		$headers = array(
			'Examinee ID',
			'First Name',
			'Last Name',
			'Middle Name',
			'Examinee Name',
			'Password',
			'Address',
			'Birthday',
			'Gender',
			'Date Added',
			'Score'
		);
		fputcsv($fp, $headers);
		
		foreach ($examinees as $key => $examinee) {
			$date_added = date('F d, Y h:sA', strtotime($examinee['Examinee']['date_added']));
			$birthday = date('F d, Y h:sA', strtotime($examinee['Examinee']['birthday']));
			$data = array(
				$examinee['Examinee']['id'],
				$examinee['User']['first_name'],
				$examinee['User']['middle_name'],
				$examinee['User']['last_name'],
				$examinee['User']['username'],
				$examinee['Examinee']['password'],
				$examinee['Examinee']['address'],
				$birthday,
				$examinee['Examinee']['gender'],
				$date_added,
				$examinee['Examinee']['score']


			);
			fputcsv($fp, $data);
		}
		
		
		fclose($fp);
		exit();
		

	}
	public function search(){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		
		$time = isset($this->request->data['Exam']['time'])?date('H:i:s', strtotime($this->request->data['Exam']['time'])):'';
		$date = isset($this->request->data['Exam']['time'])?date('Y-m-d', strtotime($this->request->data['Exam']['date'])):'';
		$schedule = $date.' '.$time;
	
		
		$conditions = "Exam.schedule LIKE '%$schedule%'";
		
		$exams = $this->Exam->find('all', compact('conditions'));
		$this->set("exams", $exams);
		
		
		
	}

	

}
;?>