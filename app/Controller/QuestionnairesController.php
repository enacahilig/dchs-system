<?php
/*all the functions for questionnaires add here*/

App::uses('AppController', 'Controller');
class QuestionnairesController extends AppController {
	public function index(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee

		$questionnaires = $this->Questionnaire->find('all');
		$this->set("questionnaires", $questionnaires);

		$fields = "Questionnaire.name";
		$questionnaire_list = $this->Questionnaire->find('list', compact("fields"));
		$questionnaire_list['new'] = "This is a new questionnaire.";
		$this->set("questionnaires_list", $questionnaire_list);

		$this->loadModel("Exam");
		$this->Exam->virtualFields['descriptionAndName'] = "CONCAT(Exam.description, ' - ', DATE_FORMAT(Exam.schedule,'%m/%d/%Y %h:%i'))";
		$fields ="descriptionAndName";
		$exams = $this->Exam->find("list", compact('fields'));
		$this->set("exams", $exams);
	}

	public function add(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee

		$this->loadModel("Item");
		if($this->request->is("post")){
			$this->Questionnaire->create();
			$this->request->data["Questionnaire"]["date_added"] = date("Y-m-d H:i");
			$this->Questionnaire->save($this->request->data);

			//save items if copied from another questionnaire
			if( $this->request->data["Questionnaire"]["questionnaires_list"]!='new'){
				$chosen_quesitonnaire= $this->request->data["Questionnaire"]["questionnaires_list"];
				$conditions = "Item.questionnaire_id=$chosen_quesitonnaire";
				$items = $this->Item->find("all", compact("conditions"));
				foreach ($items as $key => $item) {
					$this->Item->create();
					$item["Item"]["id"] = 0;
					$item["Item"]["questionnaire_id"] = $this->Questionnaire->getLastInsertId();
					/*debug($item["Item"]);*/
					$this->Item->save($item["Item"]);
				}
			}	

			$this->Session->setFlash(__('A new questionnaire was successfully added. You can now add items to the questionnaire.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect('/questionnaires');
		}
	}

	public function edit($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Questionnaire->exists($id)){
				$this->Questionnaire->id = $id;
				$this->request->data["Questionnaire"]["date_modified"] = date("Y-m-d H:i");
				$this->Questionnaire->save($this->request->data);
				$this->Session->setFlash(__('The Questionnaire was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			$this->redirect("/questionnaires");
		}
	}

	public function delete($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->Questionnaire->exists($id)){
			$this->Questionnaire->id = $id;
			$this->Questionnaire->delete();
			$this->Session->setFlash(__('The questionnaire was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/questionnaires');
	}

	public function search(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$keyword = isset($this->data['Questionnaire']['keyword'])?$this->data['Questionnaire']['keyword']:'';
		$conditions = "Questionnaire.id LIKE '%$keyword%' OR Questionnaire.name LIKE '%$keyword%'";
		$questionnaires = $this->Questionnaire->find('all', compact('conditions'));
		$this->set("questionnaires", $questionnaires);

		$this->loadModel("Exam");
		$fields = "Exam.description";
		$exams = $this->Exam->find("list", compact('fields'));
		$this->set("exams", $exams);
		
		
	}

	public function view($id){
		if($this->isAuthorized($this->Auth->user("id"))){			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee


		$questionnaire = $this->Questionnaire->findById($id);
		$this->set("questionnaire", $questionnaire);

		$this->loadModel("Item");
		$conditions = "Item.questionnaire_id=$id";
		$order = "Numbering.number ASC";	
		$items = $this->Item->find("all", compact("conditions", "order"));
		$this->set("items", $items);

		$this->loadModel("Category");
		$fields = "Category.id, Category.name";
		$categories = $this->Category->find("list", compact('fields'));
		$this->set("categories", $categories);
	}

	public function generate($id){
		if($this->isAuthorized($this->Auth->user("id"))){			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee


		$questionnaire = $this->Questionnaire->findById($id);
		$this->set("questionnaire", $questionnaire);

		$this->loadModel("Item");
		//save items in numbering and exam
		$exam_id = $questionnaire["Exam"]["id"];
		//randomize item_ids
		$conditions = "Item.questionnaire_id=$id";
		$items = $this->Item->find("list", compact("conditions"));
		shuffle($items);

		//save items in numbering 
		$this->loadModel("Numbering");

		//if numbering already done delete past numbering
		if($this->Numbering->findByQuestionnaireId($id)){
			$sql = "DELETE FROM numberings WHERE questionnaire_id=$id";
			$this->Numbering->query($sql);
		}

		$i = 0;
		foreach ($items as $numbering => $item_id) {
			$i++;
			$this->Numbering->create();
			$this->Numbering->save(
				array(
					"questionnaire_id"=>$id,
					"item_id"=>$item_id,
					"number"=>$i
				)
			);
		}
		$this->redirect("/questionnaires/view/{$id}");
	}

	public function category($id, $category_id){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee

		$questionnaire = $this->Questionnaire->findById($id);
		$this->set("questionnaire", $questionnaire);

		$this->loadModel("Item");
		$conditions = "Item.questionnaire_id=$id AND Item.category_id=$category_id";
		$items = $this->Item->find("all", compact("conditions", ""));
		$this->set("items", $items);

		$this->loadModel("Category");
		$fields = "Category.id, Category.name";
		$categories = $this->Category->find("list", compact('fields'));
		$this->set("categories", $categories);

		$fields = "Category.name";
		$conditions = "Category.id=$category_id";
		$category = $this->Category->find("first", compact('fields', 'conditions'));
		$this->set("category_name", $category["Category"]["name"]);
		$this->set("category_id", $category_id);
	}

	public function export($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		header('Content-Type: application/excel');
	    header('Content-Disposition: attachment; filename="rosetours_questionnaire.csv"');  
	    $fp = fopen('php://output', 'w');

	    $dchs = array(
        		'',
        		'',
        		'Company: DCHS',
        );

        fputcsv($fp, $dchs);

        $address = array(
        		'',
        		'',
        		'Address: Antique'
        );

        fputcsv($fp, $address);

        $questionnaire = $this->Questionnaire->findById($id);

		$questionnaire_data = array(
        		'',
        		'',
        		 $questionnaire["Questionnaire"]["name"]." ".$questionnaire["Exam"]["description"]
        );

        fputcsv($fp, $questionnaire_data);
 		fputcsv($fp, array());
        
        $headers = array(
        		'Number',
        		'Question',
        		'A',
        		'B',
        		'C',
        		'D',
        		'Answer'
        		

        );
	      
 		fputcsv($fp, $headers);
 		$this->loadModel("Item");
 		$order = "Numbering.number ASC";	
		$items = $this->Item->find("all", compact("conditions", "order"));

 		foreach ($items as $item) {
 			$data = array(
					$item['Numbering']['number'],
					$item['Item']['question'],
					$item['Item']['choice1'],
					$item['Item']['choice2'],
					$item['Item']['choice3'],
					$item['Item']['choice4'],
					$item['Item']['answer']
			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
}
;?>