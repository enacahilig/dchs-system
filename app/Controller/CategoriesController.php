<?php
/*all the functions for categories add here*/

App::uses('AppController', 'Controller');
class CategoriesController extends AppController {

	public function index(){
		//don't allow examinee
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}
		$categories = $this->Category->find("all");
		$this->set("categories", $categories);
		
	}
	public function add(){
		
		if($this->request->is("post")){
			$this->Category->create();
			$this->Category->save($this->request->data);
			$this->Session->setFlash(__('A new category was successfully added.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect('/categories');
		}



	}
	
	public function edit($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Category->exists($id)){
				$this->Category->id = $id;
				$this->Category->save($this->request->data);
				$this->Session->setFlash(__('The category was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			$this->redirect("/categories");
		}
		$category = $this->Category->findById($id);
		$this->set("category", $category);
		$this->request->data = $category;
		

	}

	public function delete($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->Category->exists($id)){
			$this->Category->id = $id;
			$this->Category->delete();
			$this->Session->setFlash(__('The category was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/categories');
	}
	public function search(){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$keyword = isset($this->data['Category']['keyword'])?$this->data['Category']['keyword']:'';
		$conditions = "Category.id LIKE '%$keyword%' OR Category.name LIKE '%$keyword%'";
		$categories = $this->Category->find('all', compact('conditions'));
		$this->set("categories", $categories);
		
		
	}

	public function view($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee

		$this->loadModel("Item");
		$conditions = "Item.category_id=$id";
		$order = "Item.id ASC ";
		$items = $this->Item->find("all", compact("conditions", "order"));
		$this->set("items", $items);

		$category = $this->Category->findById($id);
		$this->set("category", $category);
	}

	

}
;?>