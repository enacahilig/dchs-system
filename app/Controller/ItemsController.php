<?php
/*all the functions for items add here*/

App::uses('AppController', 'Controller');
class ItemsController extends AppController {
	public function add(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->request->is("post")){
			$this->Item->create();
			$this->Item->save($this->request->data);
			$this->Session->setFlash(__('A new item was successfully added.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect("/questionnaires/category/{$this->request->data['Item']['questionnaire_id']}/{$this->request->data['Item']['category_id']}");
		}
	}
	
	public function edit($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Item->exists($id)){
				$this->Item->id = $id;
				$this->Item->save($this->request->data);
				$this->Session->setFlash(__('The item was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			$this->redirect("/questionnaires/category/{$this->request->data['Item']['questionnaire_id']}/{$this->request->data['Item']['category_id']}");
		}
	}

	public function delete($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->Item->exists($id)){
			$this->Item->id = $id;
			$this->Item->delete();
			$this->Session->setFlash(__('The Item was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect($this->referer());
	}
	public function search(){
		if($this->isAuthorized($this->Auth->user("id"))){
			
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$keyword = isset($this->data['Item']['keyword'])?$this->data['Item']['keyword']:'';
		$conditions = "Item.id LIKE '%$keyword%' OR Item.name LIKE '%$keyword%'";
		$categories = $this->Item->find('all', compact('conditions'));
		$this->set("categories", $categories);
		
		
	}

	

}
;?>