<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
        'Session',
         'Auth' => array(
            'loginAction' => '/users/login',
            'logoutRedirect' => '/users/home'
        )
    );

    public function beforeFilter(){
        $this->Auth->allow("home");
    }

    public function beforeRender(){
        /*set auth here to display in default.ctp layout*/
        $this->loadModel("User");
        $auth = $this->User->read(null, AuthComponent::user("id"));
        
        $this->set("logged_user", $auth);
        $this->loadModel("Examinee");
        
        $user_id = $auth["User"]["id"];
        $conditions = "Examinee.user_id = '$user_id'";

        /*checked if logged in is examinee*/
        $is_examinee = $this->Examinee->find('first', compact('conditions'));
        $this->set('examinee_logged', $is_examinee);
       
        
    }
    // check if admin or examinee
    public function isAuthorized($id) {
        
        $this->loadModel("User");
       
        $this->loadModel("Examinee");
        
      
        $conditions = "Examinee.user_id = $id";
        $is_examinee = $this->Examinee->find('first', compact('conditions'));
    
        if ($is_examinee) {
            return true;
        }

        return false;
    }
}
