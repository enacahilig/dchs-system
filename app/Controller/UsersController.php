<?php
/*all the functions for users adre here*/

App::uses('AppController', 'Controller');
class UsersController extends AppController {
	public function login(){
		

		if ($this->request->is('post')) {

			$user_exist = $this->User->findByUsername($this->request->data['User']['username']);
			

			if ($user_exist){
				
				if($user_exist['User']['password']==$this->Auth->password($this->request->data['User']['password'])){
					
				    $this->Auth->login($user_exist['User']);
				    $this->Session->setFlash(__('You have successfully logged in.'), 'default', array('class' => 'alert alert-success'));
				   	$this->loadModel("Examinee");
				   	if($this->Examinee->findByUserId($user_exist['User']['id'])){
				   		$this->redirect("/examinees/take_exam");
				   	}
				   	else{
				    	$this->redirect("/examinees");
				   	}
				}
				else{
					
					$this->Session->setFlash(__('Invalid password. Please try again.'), 'default', array('class' => 'alert alert-danger'));
  	  			}
			}
	        else{
				$this->Session->setFlash(__('Invalid username. Please try again.'), 'default', array('class' => 'alert alert-danger'));
  	  		}
  	  	}
		
		$this->layout = "login";
	}
	public function logout(){
   		$this->redirect($this->Auth->logout());
 	}
	public function home(){
		//if loggedin redirect to dashboard
		if($this->Auth->user()){
			//if admin is logged in it goes to examinees page, if an examinee logged in and since its not authorize for accessing the /examinees it will redirect to its profile page
			$this->redirect('/examinees');
		}
		$this->layout = "home";	
	}
	
	public function account(){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		
		if($this->request->is("post") || $this->request->is("put")){
			if($this->User->exists($this->Auth->user("id"))){
				$this->User->id = $this->Auth->user("id");
				$this->User->save($this->request->data);
				$this->Session->setFlash(__('Your account was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			$this->redirect("/users/account");
		}
		$user = $this->User->findById($this->Auth->user("id"));
		$this->request->data = $user;
	}

};

?>