<?php
/*all the functions for proctors add here*/

App::uses('AppController', 'Controller');
class ProctorsController extends AppController {
	public function index(){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$proctors = $this->Proctor->find("all");
		$this->set("proctors", $proctors);

		
	}
	
	public function add(){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		
		if($this->request->is("post")){
			$this->Proctor->create();
			$this->Proctor->save($this->request->data);
			$this->Session->setFlash(__('The proctor was successfully added.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect('/proctors');
		}
		



	}
	public function view($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$proctor = $this->Proctor->findById($id);
		$this->set("proctor", $proctor);
		$this->request->data = $proctor;



	}
	public function edit($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Proctor->exists($id)){
				$this->Proctor->id = $id;
				$this->Proctor->save($this->request->data);
				$this->Session->setFlash(__('The proctor was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			$this->redirect("/proctors/view/{$id}");
		}
		$proctor = $this->Proctor->findById($id);
		$this->set("proctor", $proctor);
		$this->request->data = $proctor;
		

	}

	public function delete($id){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		if($this->Proctor->exists($id)){
			$this->Proctor->id = $id;
			$this->Proctor->delete();
			$this->Session->setFlash(__('The proctor was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/proctors');
	}

	public function search(){
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
		$keyword = isset($this->data['Proctor']['keyword'])?$this->data['Proctor']['keyword']:'';
		$conditions = "Proctor.id LIKE '%$keyword%' OR Proctor.first_name LIKE '%$keyword%' OR Proctor.last_name  LIKE '%$keyword%' OR CONCAT(Proctor.first_name,' ', Proctor.last_name)LIKE '%$keyword%' OR Proctor.middle_name OR CONCAT(Proctor.first_name,' ', Proctor.middle_name, ' ',Proctor.last_name)LIKE '%$keyword%'";
		$proctors = $this->Proctor->find('all', compact('conditions'));
		$this->set("proctors", $proctors);
		
		
	}
	public function upload_image($id){	
		if($this->isAuthorized($this->Auth->user("id"))){
			$this->loadModel('Examinee');
			$exam_id = $this->Examinee->findByUserId($this->Auth->user("id"))['Examinee']['id'];
			$this->redirect("/examinees/view/{$exam_id}");
		}//don't allow examinee
			
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));

		if ($this->request->is('post')) {
			$uploaded_image = WideImage::load($_FILES["fileToUpload"]["tmp_name"]);

			$uploaded_image->resize(200,200,'fill')->saveToFile(WWW_ROOT."img".DS."proctors".DS.$id.".jpg");
		}
		$this->redirect("/proctors/view/{$id}/refresh:".rand(1000,9999));
	}

}
;?>