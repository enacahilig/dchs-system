<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">Categories  <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addCategoryModal"));?></h1>
            
    
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('Category', array("action"=>"search", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by name or by id...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-success')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
    <div class="modal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Category", array("url"=>"/categories/add"));?>
                       
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Category Name</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter category..."));?>
                            </div>
                            
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-success"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($categories):?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                   
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($categories as $key => $category): ?>
                                <tr>
                                    <td><?php echo $category['Category']['id'];?></td>
                                    <td><?php echo $category['Category']['name'];?></td>
                                    <td>

                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editCategoryModal", "data-id"=>$category["Category"]["id"], "data-name"=>$category["Category"]["name"]));?>
                                    
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/categories/delete/{$category['Category']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?> 

                                   

                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No category added yet.
    <?php endif;?>
    <!-- Modal for Editing Van -->
    <div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Category</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Category", array("url"=>"/categories/edit", "id"=>"CategoryEditForm"));?>
                         <div class="alert alert-success">
                            <strong>Category Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Name</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter category name..."));?>
                            </div>
                             
                           
                        </div>
                      
                    </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-success"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
        
</div>
<script type="text/javascript">
    $(document).ready( function(){

        $(".editBtn").click( function   (){
            $("#CategoryEditForm #CategoryName").val($(this).attr("data-name"));

            $("#CategoryEditForm").attr("action", "<?php echo $this->base;?>/categories/edit/"+$(this).attr("data-id"));
        });
        
    });
</script> 
     

