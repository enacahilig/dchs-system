<style type="text/css">
    .answer{
        font-weight:bold;
        color:#449d44; 
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line"><?php echo $category["Category"]["name"];?>  <?php echo $this->Html->link("Add Item    <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addItem"));?></h1>
            
    
        </div>
    </div>

    <div class="modal fade" id="addItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Item</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Item", array("url"=>"/items/add"));?>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Question</label>
                                <?php echo $this->Form->text("question", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter question..."));?>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>A. First Choice</label>
                                <?php echo $this->Form->text("choice1", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first choice..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>B. Second Choice</label>
                                <?php echo $this->Form->text("choice2", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter second choice..."));?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>C. Third Choice</label>
                                <?php echo $this->Form->text("choice3", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter third choice..."));?>
                            </div>
                             <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>D. Last Choice</label>
                                <?php echo $this->Form->text("choice4", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last choice..."));?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Answer</label>
                                <?php echo $this->Form->select("answer", array("A"=>"A", "B"=>"B", "C"=>"C", "D"=>"D"),array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                            
                        </div>

                         <?php echo $this->Form->text("category_id", array("class"=>"form-control hide", "required"=>true, "value"=>$category["Category"]["id"],));?>


                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-success"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($items):?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <?php $i = 0;?>
                     <?php foreach ($items as $key => $item): ?>
                        <?php $i++;?>
                        <div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <strong><?php echo $i;?>.)  <?php echo $item['Item']['question'];?></strong>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i> Edit", "#", array("class"=>"editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editItem", "data-id"=>$item["Item"]["id"], "data-question"=>$item["Item"]["question"], "data-choice1"=>$item["Item"]["choice1"], "data-choice2"=>$item["Item"]["choice2"], "data-choice3"=>$item["Item"]["choice3"], "data-choice4"=>$item["Item"]["choice4"], "data-answer"=>$item["Item"]["answer"], "data-category-id"=>$item["Item"]["category_id"], "style"=>"font-size:10px;"));?>

                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i> Delete", "/items/delete/{$item['Item']['id']}", array("class"=>"editBtn", "escape"=>false, "style"=>"font-size:10px;", "escape"=>false));?>
                                </div>
                                <br>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='A' ? 'answer' : '';?>">
                                    A. <?php echo $item['Item']['choice1'];?><br>
                                </div>
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='B' ? 'answer' : '';?>">
                                    B. <?php echo $item['Item']['choice2'];?><br>
                                </div>
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='C' ? 'answer' : '';?>">
                                    C. <?php echo $item['Item']['choice3'];?><br>
                                </div>
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='D' ? 'answer' : '';?>">
                                    D. <?php echo $item['Item']['choice4'];?><br>
                                </div>
                            </div>
                            
                        </div>
                        
                    <?php endforeach;?>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No items added yet.
    <?php endif;?>
    <!-- Modal for Editing Questionnaire -->
    <div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Item", array("url"=>"/items/edit", "id"=>"ItemEditForm"));?>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Question</label>
                                <?php echo $this->Form->text("question", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter question..."));?>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>A. First Choice</label>
                                <?php echo $this->Form->text("choice1", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first choice..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>B. Second Choice</label>
                                <?php echo $this->Form->text("choice2", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter second choice..."));?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>C. Third Choice</label>
                                <?php echo $this->Form->text("choice3", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter third choice..."));?>
                            </div>
                             <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>D. Last Choice</label>
                                <?php echo $this->Form->text("choice4", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last choice..."));?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Answer</label>
                                <?php echo $this->Form->select("answer", array("A"=>"A", "B"=>"B", "C"=>"C", "D"=>"D"),array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                            
                        </div>

                         <?php echo $this->Form->text("category_id", array("class"=>"form-control hide", "required"=>true, "value"=>$category["Category"]["id"],));?>



                <div class="modal-footer">
                    <button type="button submit" class="btn btn-success"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
        
</div>
<script type="text/javascript">
    $(document).ready( function(){

        $(".editBtn").click( function   (){
            $("#ItemEditForm #ItemQuestion").val($(this).attr("data-question"));
            $("#ItemEditForm #ItemChoice1").val($(this).attr("data-choice1"));
            $("#ItemEditForm #ItemChoice2").val($(this).attr("data-choice2"));
            $("#ItemEditForm #ItemChoice3").val($(this).attr("data-choice3"));
            $("#ItemEditForm #ItemChoice4").val($(this).attr("data-choice4"));
            $("#ItemEditForm #ItemAnswer").val($(this).attr("data-answer"));
            $("#ItemEditForm #ItemCategoryId").val($(this).attr("data-category-id"));
           

            $("#ItemEditForm").attr("action", "<?php echo $this->base;?>/items/edit/"+$(this).attr("data-id"));
        });
        
    });
</script> 
     