<?php echo $this->Html->css("jquery-ui.theme.min");?>
<?php echo $this->Html->css("jquery-ui.structure.min");?>
<?php echo $this->Html->css("jquery-ui.min");?>

<?php echo $this->Html->css("jquery.timepicker");?>

<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Exam
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Exam", array("url"=>"/exams/add"));?>
                    <div class="alert alert-success">
                        <strong>Exam Details</strong>
                    </div>
                    
                        <div class="form-group col-md-6">
                            <label>Proctor</label>
                            <?php echo $this->Form->select("proctor_id", $proctors, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Date</label>
                            <?php echo $this->Form->text("schedule", array("class"=>"form-control", "id"=>"scheduled-date"));?>
                        </div>
                         <div class="form-group col-md-6">
                            <label>Time</label>
                            <?php echo $this->Form->text("time", array("class"=>"form-control", "id"=>"schedule_time", "required"=>true));?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Number of Examinees</label>
                            <?php echo $this->Form->number("no_of_examinees", array("class"=>"form-control", "id"=>"schedule_time", "required"=>true));?>
                        </div>

                         <div class="form-group col-md-6">
                            <label>Description</label>
                            <?php echo $this->Form->textarea("description", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter description..."));?>
                        </div>
                        <div class="col-md-12" >
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                   
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
<?php echo $this->Html->script("jquery-ui");?>
<?php echo $this->Html->script("jquery.timepicker.min");?>
<script type="text/javascript">
    $(document).ready( function(){
        $('#schedule_time').timepicker();
        $('#schedule_time').timepicker('setTime', new Date());
        $("#scheduled-date" ).datepicker({
            'minDate':new Date()
        });
    });
</script>