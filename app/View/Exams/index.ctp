<?php echo $this->Html->css("jquery-ui.theme.min");?>
<?php echo $this->Html->css("jquery-ui.structure.min");?>
<?php echo $this->Html->css("jquery-ui.min");?>

<?php echo $this->Html->css("jquery.timepicker");?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">Exams <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "/exams/add", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?></h1>
            
    
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">

            <?php echo $this->Form->create('Exam', array("action"=>"search", "id"=>"search")); ?>
           
           
            <div class="form-group col-md-4">
                <p><strong>Date:</strong></p>
                <?php echo $this->Form->text("date", array("class"=>"form-control", "id"=>"search_date"));?>
            </div>
            <div class="form-group col-md-4">
                <p><strong>Time</strong></p>
                <?php echo $this->Form->text("time", array("class"=>"form-control", "id"=>"search_time"));?>
            </div>
            <div class="form-group col-md-4">
                    <p><strong>&nbsp;</strong></p>
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-success')); ?>
            </div>
            
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($exams):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Desciption</th>
                                    <th>Proctor</th>
                                    <th>Schedule</th>
                                    <th>No. of Examinees</th>
                                    <th>Actions</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $list_of_students = 0;?>

                                <?php foreach ($exams as $key => $exam): ?>
                                <tr>
                                    

                                    <td><?php echo $exam['Exam']['id'];?></td>
                                    <td><?php echo $exam['Exam']['description'];?></td>
                                    <td><?php echo $exam['Proctor']['first_name'].' '.$exam['Proctor']['last_name'];?></td>
                                    <td><?php echo date('F d, Y h:iA', strtotime($exam['Exam']['schedule']));?></td>

                                    <td><?php echo count($exam['Examinee'])."/".$exam['Exam']['no_of_examinees'];?></td>
                                    
                                    <td>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/exams/view/{$exam["Exam"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/exams/edit/{$exam["Exam"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/exams/delete/{$exam['Exam']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No exams added yet.
    <?php endif;?>

   
        
</div>
<?php echo $this->Html->script("jquery-ui");?>
<?php echo $this->Html->script("jquery.timepicker.min");?>
<script type="text/javascript">
    $(document).ready( function(){
        $('#search_time').timepicker();
        $('#search_time').timepicker('setTime', new Date());
        $("#search_date" ).datepicker();
    });
</script>  
     

