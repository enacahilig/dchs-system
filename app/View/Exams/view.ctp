<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
   
    <div class="row">

            
        
       
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>
                        Exam Details
                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/exams/edit/{$exam["Exam"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>
                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/exams/delete/{$exam['Exam']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?","style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>

                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-download'></i>", "/exams/export_exam/{$exam['Exam']['id']}", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>
                    </h3>
                </div>
                <div class="panel-body">
                    
                        <div class="col-md-6">
                            <p>
                                <strong>Proctor:</strong>
                                <?php echo $exam['Proctor']['first_name'].' '.$exam['Proctor']['last_name'];?>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <strong>Schedule:</strong>

                                <?php echo date('F d, Y h:sA', strtotime($exam['Exam']['schedule']));?>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <strong>No. of Examinees:</strong>
                                <?php echo $exam['Exam']['no_of_examinees'];?>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <strong>Total Examinees Added:</strong>
                                <?php echo count($exam['Examinee']);?>
                            </p>
                        </div>
                        <?php if($examinees):?>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Last Name</th>
                                    <th>Examinee Username</th>
                                    <th>Password</th>
                                    <th>Address</th>
                                    <th>Birthday</th>
                                    <th>Gender</th>
                                    <th>Score</th>
                                     <th>Gender</th>
                                    <th>Actions</th>

                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($examinees as $key => $examinee):?>
                               
                                <tr>
                                    <td><?php echo $examinee['Examinee']['id'];?></td>
                                    <td><?php echo $examinee['User']['first_name'];?></td>
                                    <td><?php echo $examinee['User']['middle_name'];?></td>
                                    <td><?php echo $examinee['User']['last_name'];?></td>
                                    <td><?php echo $examinee['User']['username'];?></td>
                                    <td><?php echo $examinee['Examinee']['password'];?></td>
                                    <td><?php echo $examinee['Examinee']['address'];?></td>
                                    <td><?php echo $examinee['Examinee']['birthday'];?></td>
                                    <td><?php echo $examinee['Examinee']['gender'];?></td>
                                    <td><?php echo $examinee['Examinee']['score'];?></td>
                                    <td><?php echo $examinee['Examinee']['gender'];?></td>
                                    <td>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/examinees/view/{$examinee["Examinee"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false));?>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i>", "/examinees/scoresheet/{$examinee["Examinee"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/examinees/edit/{$examinee["Examinee"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/examinees/delete/{$examinee['Examinee']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                </tr>
                            <?php endforeach;?>

                                
                                
                            </tbody>
                        </table>
                    <?php else:?>
                        <div class="col-md-12 text-center">No examinee was added in this exam.</div>
                    <?php endif;?>

                
                    
                </div>
            </div>
        </div>
        
    </div>
    <!-- /.row -->
    

    
    
</div>