<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->meta(
        'favicon.ico',
        '/favicon.ico',
        array('type' => 'icon')
    );
    ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DCHS</title>
    <?php echo $this->Html->meta('icon'); ?>
    <?php echo $this->Html->css('bootstrap.min'); ?>
    
    <?php echo $this->Html->css('style');?>
    <?php echo $this->Html->css('custom');?>
   
    <?php echo $this->Html->script("jquery");?>

    
    
    
        
    
</head>
<style type="text/css">

</style>

<body>
<?php if( $logged_user ):?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo $this->base;?>/" style="padding-top:12px;color:white;">DCHS</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
             <?php if(!$examinee_logged):?>
                
                <li class="<?php echo $this->params['controller']=='proctors' &&  ($this->params['action']=='index' || $this->params['action']=='search' || $this->params['action']=='add'  || $this->params['action']=='view') ? 'menu-top-active' : '';?>">
                    <?php echo $this->Html->link("Proctors", "/proctors", array("escape"=>false));?>
                </li>

                <li class="<?php echo $this->params['controller']=='exams' &&  ($this->params['action']=='index' || $this->params['action']=='search' || $this->params['action']=='add'  || $this->params['action']=='view') ? 'menu-top-active' : '';?>">
                <?php echo $this->Html->link("Exams", "/exams", array("escape"=>false));?>
                </li>

                <li class="<?php echo $this->params['controller']=='examinees' &&  ($this->params['action']=='index' || $this->params['action']=='search' || $this->params['action']=='add'  || $this->params['action']=='view') ? 'menu-top-active' : '';?>">
                        <?php echo $this->Html->link("Examinees", "/examinees", array("escape"=>false));?>
                </li>

                <li class="<?php echo $this->params['controller']=='questionnaires' &&  ($this->params['action']=='index' || $this->params['action']=='search') ? 'menu-top-active' : '';?>">
                    <?php echo $this->Html->link("Questionnaires", "/questionnaires", array("escape"=>false));?>
                </li>

                 <li class="<?php echo $this->params['controller']=='categories' &&  ($this->params['action']=='index' || $this->params['action']=='search' || $this->params['action']=='add'  || $this->params['action']=='view') ? 'menu-top-active' : '';?>">
                <?php echo $this->Html->link("Categories", "/categories", array("escape"=>false));?>
                </li>

                 <li class="<?php echo $this->params['controller']=='users' &&  ($this->params['action']=='account') ? 'menu-top-active' : '';?>">
                <?php echo $this->Html->link("Account", "/users/account", array("escape"=>false));?>
                </li>
            <?php else:?>
                <li class="<?php echo $this->params['controller']=='examinees' &&   $this->params['action']=='take_exam' ? 'menu-top-active' : '' ;?>">
                    <?php echo $this->Html->link("Exam", "/examinees/take_exam/", array("escape"=>false));?>
                </li>
                <li class="<?php echo $this->params['controller']=='examinees' &&   $this->params['action']=='view' ? 'menu-top-active' : '' ;?>">
                    <?php echo $this->Html->link("Profile", "/examinees/view/{$examinee_logged['Examinee']['id']}", array("escape"=>false));?>
                </li>

            <?php endif;?>

            <li class="">
                <?php echo $this->Html->link("Log Out", "/users/logout", array("escape"=>false, "style" =>"color:white"));?>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <div class="content-wrapper" style="">
       <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer style="margin-top:160px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2015 YourCompany | By : <a href="http://www.designbootstrap.com/" target="_blank">DesignBootstrap</a>
                </div>

            </div>
        </div>
    </footer>
<?php endif;?>
    <?php echo $this->Html->script("bootstrap.min");?>
    
    

</body>
</html>
