<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Account
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("User", array("url"=>"/users/account"));?>
                    <div class="alert alert-success">
                        <strong>Account Details</strong>
                    </div>
                        
                        <div class="form-group col-md-6">
                            <label>Username</label>
                            <?php echo $this->Form->text("username", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter username..."));?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Password</label>
                            <?php echo $this->Form->password("password", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter new password...", "value"=>""));?>
                        </div>
                         <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
