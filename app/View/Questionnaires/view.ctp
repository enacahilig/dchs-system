<style type="text/css">
    .answer{
        font-weight:bold;
        color:#449d44; 
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line"><?php echo $questionnaire["Questionnaire"]["name"];?>
                <?php echo $this->Html->link("Generate Questionnaire", "/questionnaires/generate/{$questionnaire["Questionnaire"]["id"]}", array("class"=>"btn-default btn btn-default", "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>  
                <?php echo $this->Html->link("Export", "/questionnaires/export/{$questionnaire["Questionnaire"]["id"]}", array("class"=>"btn-default btn btn-default", "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>             
                <div class="pull-right">
                    <?php foreach ($categories as $id => $category):?>
                        <?php echo $this->Html->link($category, "/questionnaires/category/{$questionnaire["Questionnaire"]["id"]}/{$id}", array("class"=>"btn btn-default"));?>
                    <?php endforeach;?>
                </div>        
            </h1>   
        </div>
    </div>
    <?php if($items):?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                     <?php foreach ($items as $key => $item): ?>
                        <div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <strong><?php echo $item['Numbering']['number'];?>.)  <?php echo $item['Item']['question'];?></strong>
                                </div>
                                <br>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='A' ? 'answer' : '';?>">
                                    A. <?php echo $item['Item']['choice1'];?><br>
                                </div>
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='B' ? 'answer' : '';?>">
                                    B. <?php echo $item['Item']['choice2'];?><br>
                                </div>
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='C' ? 'answer' : '';?>">
                                    C. <?php echo $item['Item']['choice3'];?><br>
                                </div>
                                <div class="col-md-3 col-lg-3 <?php echo $item['Item']['answer']=='D' ? 'answer' : '';?>">
                                    D. <?php echo $item['Item']['choice4'];?><br>
                                </div>
                            </div>
                            
                        </div>
                        
                    <?php endforeach;?>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No items added yet.
    <?php endif;?>    
</div>

     