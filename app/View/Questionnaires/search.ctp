<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">Questionnaires  <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addQuestionnaire"));?></h1>
            
    
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('Questionnaire', array("action"=>"search", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by name or by id...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-success')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
    <div class="modal fade" id="addQuestionnaire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Questionnaire</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Questionnaire", array("url"=>"/questionnaires/add"));?>
                       
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Questionnaire</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter Questionnaire..."));?>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Exam</label>
                                <?php echo $this->Form->select("exam_id", $exams, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                            
                            
                        </div>
                         <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Copy Questionnaire from:</label>
                                <?php echo $this->Form->select("questionnaires_list", $questionnaires_list, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                            
                            
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-success"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($questionnaires):?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Questionnaire</th>
                                    <th>No. of Items</th>
                                    <th>Exam</th>
                                    <th>Date Added</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($questionnaires as $key => $questionnaire): ?>
                                <tr>
                                    <td><?php echo $questionnaire['Questionnaire']['id'];?></td>
                                    <td><?php echo $questionnaire['Questionnaire']['name'];?></td>
                                      <td><?php echo count($questionnaire['Item']);?></td>
                                    <td><?php echo $questionnaire['Exam']['description'];?></td>
                                     <td><?php echo date('F d, Y h:iA', strtotime($questionnaire['Questionnaire']['date_added']));?></td>                                    <td>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i>", "/questionnaires/view/{$questionnaire['Questionnaire']['id']}", array("class"=>"btn btn-default btn-primary", "escape"=>false));?>

                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editQuestionnaire", "data-id"=>$questionnaire["Questionnaire"]["id"], "data-name"=>$questionnaire["Questionnaire"]["name"], "data-exam-id"=>$questionnaire["Exam"]["id"]));?>
                                    
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/questionnaires/delete/{$questionnaire['Questionnaire']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                   

                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No questionnaire added yet.
    <?php endif;?>
    <!-- Modal for Editing Questionnaire -->
    <div class="modal fade" id="editQuestionnaire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Questionnaire</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Questionnaire", array("url"=>"/questionnaires/edit", "id"=>"QuestionnaireEditForm"));?>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Questionnaire</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter Questionnaire..."));?>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Exam</label>
                                <?php echo $this->Form->select("exam_id", $exams, array("class"=>"form-control", "required"=>true, 'empty'=>false));?>
                            </div>
                            
                            
                        </div>
                      
                    </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-success"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
        
</div>
<script type="text/javascript">
    $(document).ready( function(){

        $(".editBtn").click( function   (){
            $("#QuestionnaireEditForm #QuestionnaireName").val($(this).attr("data-name"));
            $("#QuestionnaireEditForm #QuestionnaireExamId").val($(this).attr("data-exam-id"));
           

            $("#QuestionnaireEditForm").attr("action", "<?php echo $this->base;?>/questionnaires/edit/"+$(this).attr("data-id"));
        });
        
    });
</script> 
     