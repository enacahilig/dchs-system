<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
   
    <div class="row">
        
            
        
        <div class="col-md-3 col-lg-3 text-center">
            <?php $file = WWW_ROOT."img".DS."proctors" .DS.$proctor["Proctor"]["id"].".jpg";?>
            <?php if(file_exists($file)):?>
                <?php echo $this->Html->image("proctors/{$proctor["Proctor"]["id"]}.jpg", array("class"=>"img-thumbnail"));?>
            <?php else:?>
                <img src="http://placehold.it/200x200"/>
            <?php endif;?>
            
            <div class="clearfix"><br></div>
             <?php echo $this->Html->link("Change Photo <i class='glyphicon glyphicon-camera'></i>", "#", array("class"=>"btn btn-default btn-success", "escape"=>false, "data-toggle"=>"modal","data-target"=>"#uploadPhoto" ));?>
            <div class="clearfix"><br></div>

             <!-- Modal for upload photo -->
             <div class="modal fade" id="uploadPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload Photo</h4>
                      </div>
                      <div class="modal-body">
                        <form enctype="multipart/form-data" action="<?php echo $this->base;?>/proctors/upload_image/<?php echo $proctor['Proctor']['id'] ?>" method="post">
                            <div class="form-group">
                                <input id="file" class="file" name="fileToUpload" type="file">
                            </div>
                        </form>
                        <div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          
        </div>
        <div class="col-md-8" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>
                        <?php $s = substr($proctor["Proctor"]["first_name"], -1) != "s" ? "s" : "" ;?>
                        <?php echo "{$proctor["Proctor"]["first_name"]}'{$s} Profile" ;?>
                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/proctors/edit/{$proctor["Proctor"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>
                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/proctors/delete/{$proctor['Proctor']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?","style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>
                    </h3>
                </div>
                <div class="panel-body">
                    
                        <p>
                            <strong>First Name:</strong>
                            <?php echo $proctor["Proctor"]["first_name"] ;?>
                        </p> 
                        <p>
                            <strong>Middle Name:</strong>
                            <?php echo $proctor["Proctor"]["middle_name"];?>
                        </p>
                        <p>
                            <strong>Last Name:</strong>
                            <?php echo $proctor["Proctor"]["last_name"];?>
                        </p>
                
                
                    
                </div>
            </div>
        </div>
        
    </div>
    <!-- /.row -->
    

    
    
</div>