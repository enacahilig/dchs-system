<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">Proctors <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "/proctors/add", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?></h1>
            
    
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('Proctor', array("action"=>"search", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by name or by id...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-success')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($proctors):?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Last Name</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($proctors as $key => $proctor): ?>
                                <tr>
                                    <td><?php echo $proctor['Proctor']['id'];?></td>
                                    <td><?php echo $proctor['Proctor']['first_name'];?></td>
                                    <td><?php echo $proctor['Proctor']['middle_name'];?></td>
                                    <td><?php echo $proctor['Proctor']['last_name'];?></td>
                                     <td>
                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/proctors/view/{$proctor["Proctor"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false));?>
                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/proctors/edit/{$proctor["Proctor"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>
                                     <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/proctors/delete/{$proctor['Proctor']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                   

                                </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
     <?php else:?>
        No proctor added yet.
    <?php endif;?>
        
</div>
   
     

