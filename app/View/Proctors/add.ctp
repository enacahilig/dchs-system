<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Proctor
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Proctor", array("url"=>"/proctors/add"));?>
                    <div class="alert alert-success">
                        <strong>Proctor Details</strong>
                    </div>
                    
                        <div class="form-group col-md-6">
                            <label>First Name</label>
                            <?php echo $this->Form->text("first_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first name..."));?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Middle Name</label>
                            <?php echo $this->Form->text("middle_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter middle name..."));?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Last Name</label>
                            <?php echo $this->Form->text("last_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last name..."));?>
                        </div
                        >
                        <div class="col-md-12" >
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                   
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>