<style type="text/css">
	.radio input[type=radio], .radio-inline input[type=radio]{
		margin-top: 10px;
	}


</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			  <h1 class="page-head-line">Start Time:<?php echo date("h:iA", strtotime($examinee["Examinee"]["start_time"]));?>
			  <span  class="pull-right"> Time Remaining: <span id="clock"></span></span></h1>
		</div>
	    <div class="row">
	    	<div id="status">
	    		
	    	</div>
	        <div class="col-md-12">
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                	Schedule: <?php echo date("F d,Y h:iA", strtotime($examinee["Exam"]["schedule"]));?>
	                    <br/>
	                </div>
	                <div class="panel-body">	
	                     <?php foreach ($items as $key => $item): ?>
	                        <div>
	                            <div class="row">
	                                <div class="col-lg-12">
	                                    <strong><?php echo $item['Numbering']['number'];?>.)  <?php echo $item['Item']['question'];?></strong>
	                                </div>
	                                <br>
	                            </div>
	                            <div class="row">
	                            	<div class="col-lg-12">
		                                <label data-item-id="<?php echo $item['Item']['id'];?>" data-choice="A" " data-id="answer-<?php echo $item['Item']['id'];?>-A" class="radio-inline">
		                                	<input data-item-id="<?php echo $item['Item']['id'];?>" class="radio" data-choice="A" type="radio"  id="answer-<?php echo $item['Item']['id'];?>-A" value="A" name="answer-<?php echo $item['Item']['id'];?>"> A. <?php echo $item['Item']['choice1'];?> 
		                                	<br>
		                                </label>
										<label  data-item-id="<?php echo $item['Item']['id'];?>" data-choice="B" " data-id="answer-<?php echo $item['Item']['id'];?>-B"class=" radio-inline">
											<input  class="radio"  data-choice="B" type="radio"  value="A" id="answer-<?php echo $item['Item']['id'];?>-B" value="B" name="answer-<?php echo $item['Item']['id'];?>" data-item-id="<?php echo $item['Item']['id'];?>">
											B. <?php echo $item['Item']['choice2'];?>
											<br>
										</label>
										<label  data-item-id="<?php echo $item['Item']['id'];?>" data-choice="C" " data-id="answer-<?php echo $item['Item']['id'];?>-C" class=" radio-inline">
											<input   class="radio" data-choice="C"  type="radio"  value="A" id="answer-<?php echo $item['Item']['id'];?>-C" value="C" name="answer-<?php echo $item['Item']['id'];?>" data-item-id="<?php echo $item['Item']['id'];?>"> 
											C. <?php echo $item['Item']['choice3'];?>
											<br>
										</label>
										<label  data-item-id="<?php echo $item['Item']['id'];?>" data-choice="D" " data-id="answer-<?php echo $item['Item']['id'];?>-D" class=" radio-inline">
											<input  class="radio" data-choice="D" type="radio"  value="A" id="answer-<?php echo $item['Item']['id'];?>-D" value="D" name="answer-<?php echo $item['Item']['id'];?>" data-item-id="<?php echo $item['Item']['id'];?>"> D. <?php echo $item['Item']['choice4'];?>
											<b>
										</label>
									</div>
	                            </div>
	                            
	                        </div>
	                        
	                    <?php endforeach;?>
	                    <?php echo 	$this->Html->link("Submit", "/examinees/submit", array("class"=>"btn btn-lg btn-primary pull-right",  "confirm"=>"Are you sure?"));?>
	                </div>
	            </div>
	                   
	        </div>
	    </div>
	</div>
</div>
<?php echo $this->Html->script("jquery.countdown");?>
<script type="text/javascript">

	$(document).ready( function(){	

		$('#clock').countdown("<?php echo $end_date;?>", function(event) {
		   	var totalHours = event.offset.totalDays * 24 + event.offset.hours;
		   	$(this).html(event.strftime(totalHours + ' hr %M min %S sec'));

	 	})
	 	.on('finish.countdown', function(event) {
		  	window.location.href = "<?php echo $this->base;?>" + "/examinees/submit";
		});

		var saved_answers = JSON.parse('<?php echo isset($examinee["Answer"]["answers"]) ? $examinee["Answer"]["answers"] : 0 ;?>');
		console.log(saved_answers);

		$.each(saved_answers, function(id, choice){
			$("#answer-"+id+"-"+choice).prop("checked", true);
		});

	 	var answers = {};
		$(".radio-inline input").click( function (e){
			if($('#answer-'+$(this).attr("data-item-id")+'-'+ $(this).attr("data-choice")).is(':checked')){
				answers[$(this).attr("data-item-id")] = $(this).attr("data-choice");
			}
			
			//pass answers here
			$.ajax({
				method:"POST",
				url: "<?php echo $this->base;?>/examinees/ajax_save_answers/<?php echo $examinee['Examinee']['id'];?>",
				data:{
					answers:JSON.stringify(answers)
				},
				success: function(result) {
					$("#status").html(result);
				}
			});
		});		
	});
</script>
