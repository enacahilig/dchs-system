<style type="text/css">
 	.correct_answer{
    	color:red;
    	font-weight:bold;
    }

    .answer{
        font-weight:bold;
        color:#449d44; 
    }

</style>
<div class="container-fluid">
	<div class="row">
		<?php if($questionnaire["Item"]):?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="panel panel-default">
		                <div class="panel-heading">
		               		<h1 class="page-head-line">
			               		Schedule: <?php echo date("F d,Y h:iA", strtotime($examinee["Exam"]["schedule"]));?>
			                	<span class="pull-right"><?php echo "Score: {$score}/{$total_items}";?></span>	
		                	</h1>
		                	<?php foreach($display_scores as $category=>$scores):?>
								<h5><?php echo $category;?>:<?php echo $scores;?></h5>
								<br>
							<?php endforeach;?>
		                </div>
		                <div class="panel-body">
		                     <?php foreach ($questionnaire["Item"] as $key => $item): ?>
		                        <div>
		                            <div class="row">
		                                <div class="col-lg-12">
		                                	<?php echo $item['exam_answer']==$item["answer"] ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>';?>
		                                    <strong><?php echo $questionnaire["Numbering"][$key]['number'];?>.)  <?php echo $item['question'];?></strong>	
		                                </div>
		                                <br>
		                            </div>
		                            <div class="row">
		                                <div class="<?php echo 'A'==$item["answer"] ? 'correct_answer' : '';?> col-md-3 col-lg-3 <?php echo $item['exam_answer']=='A' ? 'answer' : '';?>">
		                                    A. <?php echo $item['choice1'];?><br>
		                                </div>
		                                <div class=" <?php echo 'B'==$item["answer"] ? 'correct_answer' : '';?> col-md-3 col-lg-3 <?php echo $item['exam_answer']=='B' ? 'answer' : '';?>">
		                                    B. <?php echo $item['choice2'];?><br>
		                                </div>
		                                <div class="<?php echo 'C'==$item["answer"] ? 'correct_answer' : '';?>  col-md-3 col-lg-3 <?php echo $item['exam_answer']=='C' ? 'answer' : '';?>">
		                                    C. <?php echo $item['choice3'];?><br>
		                                </div>
		                                <div class="<?php echo 'D'==$item["answer"] ? 'correct_answer' : '';?>  col-md-3 col-lg-3 <?php echo $item['exam_answer']=='D' ? 'answer' : '';?> ">
		                                    D. <?php echo $item['choice4'];?><br>
		                                </div>
		                            </div>
		                            
		                        </div>
		                        
		                    <?php endforeach;?>
		                </div>
		            </div>
		                   
		        </div>
		    </div>
		    <?php else:?>
		        No items added yet.
		    <?php endif;?>    
		
	</div>
		
</div>