<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">Examinees <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "/examinees/add", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?></h1>
            
    
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('Examinee', array("action"=>"search", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by name...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-success')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($examinees):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Last Name</th>
                                    <th>Examinee Name</th>
                                    <th>Password</th>
                                    <th>Address</th>
                                    <th>Birthday</th>
                                    <th>Gender</th>
                                    <th>Score</th>
                                    <th>Actions</th>

                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($examinees as $key => $examinee): ?>
                                <tr>
                                    <td><?php echo $examinee['Examinee']['id'];?></td>
                                    <td><?php echo $examinee['User']['first_name'];?></td>
                                    <td><?php echo $examinee['User']['middle_name'];?></td>
                                    <td><?php echo $examinee['User']['last_name'];?></td>
                                    <td><?php echo $examinee['User']['username'];?></td>
                                    <td><?php echo $examinee['Examinee']['password'];?></td>
                                    <td><?php echo $examinee['Examinee']['address'];?></td>
                                    <td><?php echo $examinee['Examinee']['birthday'];?></td>
                                    <td><?php echo $examinee['Examinee']['gender'];?></td>
                                    <td><?php echo $examinee['Examinee']['score'];?></td>
                                    <td>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/examinees/view/{$examinee["Examinee"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i>", "/examinees/scoresheet/{$examinee["Examinee"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/examinees/edit/{$examinee["Examinee"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/examinees/delete/{$examinee['Examinee']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No examinee added yet.
    <?php endif;?>

   
        
</div>
   
     

