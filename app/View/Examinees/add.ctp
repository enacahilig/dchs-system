<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Examinee
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Examinee", array("url"=>"/examinees/add"));?>
                    <div class="alert alert-success">
                        <strong>Examinee Details</strong>
                    </div>
                        
                        <div class="form-group col-md-4">
                            <label>First Name</label>
                            <?php echo $this->Form->text("first_name", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter first name..."));?>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Middle Name</label>
                            <?php echo $this->Form->text("middle_name", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter middle name..."));?>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Last Name</label>
                            <?php echo $this->Form->text("last_name", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter last name..."));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>User Name</label>
                            <?php echo $this->Form->text("user_name_display", array("class"=>"form-control", "required"=>true, "disabled"=>true));?>
                            <?php echo $this->Form->text("user_name", array("class"=>"form-control hide", "required"=>true));?>
                        </div>
                         <div class="form-group col-md-6 col-lg-6">
                            <label>Password</label>
                            <?php echo $this->Form->text("password_display", array("class"=>"form-control", "required"=>true,  "disabled"=>true));?>
                            <?php echo $this->Form->password("password", array("class"=>"form-control hide", "required"=>true));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Address</label>
                            <?php echo $this->Form->text("address", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter address..."));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Birthday</label>
                            <br>
                             <?php echo $this->Form->select("day",null,array("class"=>"form-control ",  "id"=>"day","required"=>true, "empty"=>false));?>
                            <?php echo $this->Form->select("month",null,array("class"=>"form-control",  "id"=>"month","required"=>true, "empty"=>false));?>
                           
                            <?php echo $this->Form->select("year",null,array("class"=>"form-control",  "id"=>"year","required"=>true, "empty"=>false));?>
                           
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Gender</label>
                            <?php echo $this->Form->select("gender",array("M"=>"Male", "F"=>"Female"),  array("class"=>"form-control",  "required"=>true,"empty"=>false));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Exam Schedule</label>
                            <?php echo $this->Form->select("exam_id", $schedules,  array("class"=>"form-control",  "required"=>true,"empty"=>false));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Status</label>
                            <?php echo $this->Form->select("status",array("Active"=>"Active", "Not Active"=>"Not Active"), array("class"=>"form-control", "required"=>true,"empty"=>false));?>
                        </div>
                        <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
<?php echo $this->Html->script("dobPicker.min");?>
<script type="text/javascript">
    $(document).ready( function(){
        $.dobPicker({
            daySelector: '#day', /* Required */
            monthSelector: '#month', /* Required */
            yearSelector: '#year', /* Required */
            minimumAge: 8, /* Optional */
            maximumAge: 100 /* Optional */
        });

        $(".name").change( function(){
            var user_name = $("#ExamineeFirstName").val() + "." + $("#ExamineeLastName").val() + "_" + Math.random().toString(36).substr(2, 5);
            var password = Math.random().toString(36).substr(2, 6);
            $("#ExamineeUserNameDisplay").val(user_name);
             $("#ExamineePasswordDisplay").val(password);
            $("#ExamineePassword").val(password);
            $("#ExamineeUserName").val(user_name);
        });
    });   
</script>
