<?php
App::uses('AppModel', 'Model');
/**
 * Questionnaire Model
 *
 */
class Item extends AppModel {
	public $belongsTo =  array(
		'Questionnaire' => array(
			'className' => 'Questionnaire',
		),
		'Category' => array(
			'className' => 'Category',
		)
	);

	public $hasOne = array(
		'Numbering'=> array(
			'className' => 'Numbering',
		)
	);
}
;?>