<?php
App::uses('AppModel', 'Model');
/**
 * Questionnaire Model
 *
 */
class Questionnaire extends AppModel {
	public $hasMany = array(
		'Item' => array(
			'className' => 'Item',
		),
		'Numbering' => array(
			'className' => 'Numbering',
		)
	);
	public $belongsTo =  array(
		'Exam' => array(
			'className' => 'Exam',
		)
	);
}
;?>