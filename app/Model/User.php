<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {	
	public $hasOne = array(
		
		'Examinee' => array(
			'className' => 'Examinee',
		),
		
	);

	/*cake way of saving secured password*/
	public function beforeSave($options = array()) {
	    if (isset($this->data[$this->alias]['password'])) {
	        $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
	    }
   		return true;
	}

	

}
;?>
