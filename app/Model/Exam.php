<?php
App::uses('AppModel', 'Model');
/**
 * Exam Model
 *
 */
class Exam extends AppModel {
	public $hasMany = array(
		
		'Examinee' => array(
			'className' => 'Examinee',
		),
		'Questionnaire' => array(
			'className' => 'Questionnaire',
		)
	);

	public $belongsTo =  array(
		'Proctor' => array(
			'className' => 'Proctor',
		)
	);
	

}
;?>