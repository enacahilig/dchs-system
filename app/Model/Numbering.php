<?php
App::uses('AppModel', 'Model');
/**
 * Numbering Model
 *
 */
class Numbering extends AppModel {
	public $belongsTo =  array(
		'Questionnaire' => array(
			'className' => 'Questionnaire',
		),
		'Item' => array(
			'className' => 'Item',
		)
	);
}
;?>