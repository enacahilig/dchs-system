<?php
App::uses('AppModel', 'Model');
/**
 * Examinee Model
 *
 */
class Examinee extends AppModel {
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
		),
		'Exam' => array(
			'className' => 'Exam',
		)
	);

	public $hasOne = array(
		'Answer' => array(
			'className' => 'Answer',
		)
	);



}
;?>